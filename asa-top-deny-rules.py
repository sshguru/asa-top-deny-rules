#!/usr/bin/env python

# import Python modules needed for the script
import re
import sys
import os
import time
from subprocess import Popen, PIPE
from shlex import split

# start measuring time
start_time=time.time()

# prepare files needed for the script
# file containing ASA logs
# note: saved as string for 1st Popen call
LogFile = "grep denied " + sys.argv[1]
# target/result file
ResFile = sys.argv[2]
# temp file for processing
TmpFile = open("bpfilter.tmp", 'w+')

# empty list for saving deny rules hits
HitCons = []

# use Bash grep and awk to filter the original ASA log
# to do this in Python, we call three new subprocesses
print "Preparing to run the script - this will take 2-3min"
p1 = Popen(split(LogFile), stdout=PIPE)
p2 = Popen(split("awk '{print $11, $13}'"), stdin=p1.stdout, stdout=PIPE)
p3 = Popen(split("grep vlan"), stdin=p2.stdout, stdout=TmpFile)
# last grep for vlan is just a sanity check

# wait for above processes to complete
p3.communicate()

# reload the TmpFile after Bash is done
TmpFile.close()
TmpFile = open("bpfilter.tmp", 'r')

# display status update
print "Initiating..."

# get number of logs to process for tracking
# stat -> current progress
stat = 0
goal = sum(1 for i in open("bpfilter.tmp", 'rb')) # total number of logs
# goal -> total number of logs

# display status update
print "Filtering progress"

# this part processes the filtered log file line-by-line (or log-by-log if you will)
for log in TmpFile:

    # display status update
    if stat % 250000 == 0:
        print stat, "/", goal

    # save left side of a connection string to a variable
    lHost = log.split(" ")[0]
    # save right side of a connection string to a variable
    rHost = log.split(" ")[1]

    # extract ports from both side of the connection string
    lPort = lHost.split(")")[0].split("(")[1]
    rPort = rHost.split(")")[0].split("(")[1]

    # check if port variables aren't empty and if left side port is bigger than right side port
    if int(lPort) > int(rPort):

        # if left side port is bigger, remove port number from left side connection string
        # and save it as lHost --> rHost/rPort
        HitCons.append(lHost.split("(")[0] + "-->" + rHost)
        stat += 1

    else:

        # if right side port is bigger, remove port number from right side connection string
        # and save it as rHost --> lHost/lPort
        HitCons.append(rHost.split("(")[0] + "-->" + lHost)
        stat += 1

# display user update
print goal, "/", goal

# reload and write filtered list to tmp filte
TmpFile.close()
TmpFile = open("bpfilter.tmp", 'w+')
for log in HitCons:
    TmpFile.write(log+"\n")

# that's pretty mcuh same thing as:
# bash # sed -i "/^\s*$/d" bpfilter.tmp > sort -o > uniq -c > result && sort -rn result
print "Calculating top deny rules"
os.system('sed -i "/^\s*$/d" bpfilter.tmp')
print "25%"
os.system("sort -o bpfilter.tmp bpfilter.tmp")
print "50%"
os.system("uniq -c bpfilter.tmp" + "> " + ResFile)
print "75%"
os.system("sort -rn -o " + ResFile + " " + ResFile)
print "100%"

# clean tmp file
os.system("rm bpfilter.tmp")

# display total time in minutes
end_time = time.time() - start_time
end_time = end_time / 60
print "Total run time: ", str(end_time) + " minutes."